


public class Start {

	public static void main(String[] args) {
		View v = new View();
		Controller c = new Controller(v);
		v.frame.setVisible(true);
	}

}
