
import java.util.*;


public class Queue extends Thread{
	private ArrayList<Client> queue;
	private String name;
	private int empty = 0;
	private int clients = 0;
	
	public Queue(String name){
		queue = new ArrayList<Client>();
		setName(name);
	}

	public Queue(){
		ArrayList<Client> queue = new ArrayList<Client>();
		this.queue = queue;
	}

	public void run() {
		
		while(true){
			if(!queue.isEmpty()){
				try {
					if(Scheduler.getTime() == (queue.get(0).getArrivet() + queue.get(0).getServicet())){
						sleep(100);
						//deleteClient();
						while (queue.size() == 0)
							wait();
						//System.out.println("The client " + queue.get(0).getID() + " left from queue " + getName() + " at the time " + Scheduler.getTime());
						String s = "The client " + queue.get(0).getID() + " left from queue " + getName() + " at the time " + Scheduler.getTime();
						System.out.println(s);
						queue.remove(0);
						Queue queues[] = Scheduler.getQueues();
						String string= "";
						String line = ""; String queue[] = new String[40];
						for(int i = 0; i < Scheduler.getQueuenr(); i++){
							line = "";
							if((i+1) == Integer.parseInt(getName()))
								for(Client c: this.queue)
									line += c.getID() + " ";
							else
								for(Client c: queues[i].getQueue())
									line += c.getID() + " ";
							queue[i] = "Queue " + (i+1) + ": " + line;
							string += queue[i] + '\n';
						}
						//System.out.println(string);
						Controller.getView().animation.setText(string);
						Controller.getView().log.append(s + '\n');
					}
					//sleep(queue.get(0).getArrivet() * 1000);	
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			else{
				if (Scheduler.getTime() < Scheduler.getTerminate() && queue.isEmpty()){
					empty++;
				}
				try {
					sleep(500);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
		
	}
	
	public synchronized int emptyQueue(){
		return empty;
	}
	
	public synchronized ArrayList<Client> getQueue(){
		return this.queue;
	}
	
	public synchronized void addClient(Client c){
		queue.add(c);
		notifyAll();
	}
	
	public synchronized int getWaitingt(){
		int waitingt = 0;
		for (Client c: queue){
			waitingt += c.getServicet();
		}
		return waitingt;
	}
}
