package gui;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import Polynomials.Operations;
import Polynomials.Polynomial;

public class Controller {
	private Interface inter;
	private Polynomial p1, p2;
	private Operations op = new Operations();
	
	public Controller(Interface inter){
		this.inter = inter;
		inter.addPol1Listener(new actionPol1());
		inter.addPol2Listener(new actionPol2());
		inter.addAddListener(new actionAdd());
		inter.addSubListener(new actionSub());
		inter.addMulListener(new actionMul());
		inter.addDerListener(new actionDer());
		inter.addDivListener(new actionDiv());
		inter.addIntegrListener(new actionIntegr());
	}
	
	class actionPol1 implements ActionListener{
		public void actionPerformed(ActionEvent a) {
			try{
			String poly1 = inter.pol1.getText();
			System.out.println(poly1);
			p1 = new Polynomial(poly1);
			} catch(Exception e){
				Component frame = null;
				JOptionPane.showMessageDialog(frame  , "The 1st polynomial was not entered corectly!");
			}
				
		}
	}
	
	class actionPol2 implements ActionListener{
		public void actionPerformed(ActionEvent a) {
			try{
			String poly2 = inter.pol2.getText();
			System.out.println(poly2);
			p2 = new Polynomial(poly2);
			} catch(Exception e){
				Component frame = null;
				JOptionPane.showMessageDialog(frame  , "The 2nd polynomial was not entered corectly!");
			}
		}
	}
	
	class actionAdd implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			Polynomial p3 = op.addPol(p1, p2);
			inter.pol3.setText(p3.toString());
		}
		
	}
	
	class actionSub implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			Polynomial p3 = op.subPol(p1, p2);
			inter.pol3.setText(p3.toString());
		}
		
	}
	
	class actionMul implements ActionListener{
		public void actionPerformed(ActionEvent arg0) {
			Polynomial p3 = op.mulPol(p1, p2);
			inter.pol3.setText(p3.toString());
			
		}
		
	}
		
	class actionDer implements ActionListener{
		public void actionPerformed(ActionEvent arg0) {
			Polynomial p3 = op.derPol(p1);
			inter.pol3.setText(p3.toString());
			
		}
		
	}
	
	class actionDiv implements ActionListener{
		public void actionPerformed(ActionEvent arg0) {
			Component frame = null;
			if(p1.P.get(0).getGrade() < p2.P.get(0).getGrade() )
				JOptionPane.showMessageDialog(frame , "P1 must be greater than P2 !");
			else{
			Polynomial[] p3 = op.divPol(p1, p2);
			String s = "Quotient: ";
			for (int i = 0; i < 2; i++){
				s = s + p3[i].toString();
				if (i == 0)
					s = s + " Remainder: ";
			}
			inter.pol3.setText(s);
			}
		}
		
	}
	
	class actionIntegr implements ActionListener{
		public void actionPerformed(ActionEvent arg0) {
			Polynomial p3 = op.integrPol(p1);
			inter.pol3.setText(p3.toString());
			
		}
		
	}
	
}
