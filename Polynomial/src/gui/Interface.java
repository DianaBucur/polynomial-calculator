package gui;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class Interface extends JFrame{
	public JFrame frame = new JFrame("Polynomials");
	JLabel textp1 = new JLabel("Enter the 1st polynomial: ");
	JLabel textp2 = new JLabel("Enter the 2nd polynomial: ");
	JLabel textp3 = new JLabel("The result is: ");
	JLabel rule = new JLabel("Polynomials should be: +-cnx^n+-c(n-1)x^(n-1)...+-c1x^1+-c0");
	JTextField pol1 = new JTextField(25);
	JTextField pol2 = new JTextField(25);
	JTextField pol3 = new JTextField(40);
	JButton add = new JButton("+");
	JButton sub = new JButton("-");
	JButton mul = new JButton("*");
	JButton div = new JButton("/");
	JButton der = new JButton("'");
	JButton integr = new JButton("∫");
	JPanel panel1 = new JPanel();
	JPanel panel2 = new JPanel();
	JPanel panel3 = new JPanel();
	JPanel panel4 = new JPanel();
	JPanel panel5 = new JPanel();
	
	public Interface(){
		initialize();
	}
	
	public void initialize(){
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(500, 500);
		pol3.setHorizontalAlignment(JTextField.CENTER);
		
		panel5.add(rule);
		panel3.add(textp1);
		panel3.add(pol1);
		panel1.add(textp2);
		panel1.add(pol2);
		panel2.add(add);
		panel2.add(sub);
		panel2.add(mul);
		panel2.add(div);
		panel2.add(der);
		panel2.add(integr);
		panel4.add(textp3);
		panel4.add(pol3);
		
		JPanel p = new JPanel();
		p.add(panel5);
		p.add(panel3);
		p.add(panel1);
		p.add(panel2);
		p.add(panel4);
		
		p.setLayout(new BoxLayout(p, BoxLayout.Y_AXIS));
		frame.setContentPane(p);
		frame.setVisible(true);
	}
	
	public void addPol1Listener(ActionListener a){
		pol1.addActionListener(a);
	}
	
	public void addPol2Listener(ActionListener a){
		pol2.addActionListener(a);
	}
	
	public void addAddListener(ActionListener a){
		add.addActionListener(a);
	}
	
	public void addSubListener(ActionListener a){
		sub.addActionListener(a);
	}
	
	public void addMulListener(ActionListener a){
		mul.addActionListener(a);
	}
	
	public void addDerListener(ActionListener a){
		der.addActionListener(a);
	}
	
	public void addDivListener(ActionListener a){
		div.addActionListener(a);
	}
	
	public void addIntegrListener(ActionListener a){
		integr.addActionListener(a);
	}
	
}
