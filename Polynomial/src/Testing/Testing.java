package Testing;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

import Polynomials.Operations;
import Polynomials.Polynomial;

public class Testing {
	
	@Test
	public void test() {
		Operations op = new Operations();
		String[] s1 = {"+16x^3+5x^2+5", "-5x^2+9", "-15x^5+3"};
		String[] s2 = {"+6x^2-4x^1+1", "+14x^1", "+3x^2+9"};
		String[] output = {"16x^3+11x^2-4x^1+6", "-5x^2+14x^1+9", "-15x^5+3x^2+12"};
		for (int i = 0; i < 3; i++){
			Polynomial p1 = new Polynomial(s1[i]);
			Polynomial p2 = new Polynomial(s2[i]);
			Polynomial p3 = op.addPol(p1,p2);
			assertEquals(p3.toString(), output[i]);
		}
	}
	
	@Test
	public void test2() {
		Operations op = new Operations();
		String[] s1 = {"+16x^3+5x^2+5", "-5x^2+9", "-15x^5+3"};
		String[] s2 = {"+6x^2-4x^1+1", "+14x^1", "+3x^2+9"};
		String[] output = {"16x^3-1x^2+4x^1+4", "-5x^2-14x^1+9", "-15x^5-3x^2-6"};
		for (int i = 0; i < 3; i++){
			Polynomial p1 = new Polynomial(s1[i]);
			Polynomial p2 = new Polynomial(s2[i]);
			Polynomial p3 = op.subPol(p1,p2);
			assertEquals(p3.toString(), output[i]);
		}
	}
	
	@Test
	public void test3() {
		Operations op = new Operations();
		String[] s1 = {"+16x^3+5x^2+5", "-5x^2+9", "-15x^5+3"};
		String[] s2 = {"+6x^2-4x^1+1", "+14x^1", "+3x^2+9"};
		String[] output = {"96x^5-1920x^4-320x^3+150x^2-20x^1+5", "-70x^3+126x^1", "-45x^7-135x^5+9x^2+27"};
		for (int i = 0; i < 3; i++){
			Polynomial p1 = new Polynomial(s1[i]);
			Polynomial p2 = new Polynomial(s2[i]);
			Polynomial p3 = op.mulPol(p1,p2);
			assertEquals(p3.toString(), output[i]);
		}
	}
	
	@Test
	public void test4() {
		Operations op = new Operations();
		String[] s1 = {"+6x^2+3x^1", "-5x^2+9", "-15x^5+3"};
		String[] s2 = {"+3x^1", "+5x^1", "+3x^2+9"};
		String[] output = {"Quotient: 2x^1+1 Remainder: ", "Quotient: -1x^1 Remainder: +9", "Quotient: -5x^3+15x^1 Remainder: -135x^1+3"};
		for (int i = 0; i < 1; i++){
			Polynomial p1 = new Polynomial(s1[i]);
			Polynomial p2 = new Polynomial(s2[i]);
			Polynomial[] p3 = op.divPol(p1,p2);
			String s = "Quotient: ";
			for (int j = 0; j < 2; j++){
				s = s + p3[j].toString();
				if (j == 0)
					s = s + " Remainder: ";
			}
			assertEquals(s, output[i]);
		}
	}
	
	@Test
	public void test5() {
		Operations op = new Operations();
		String[] s1 = {"+6x^6-16x^4+2x^2+3x^1", "-5x^2+9", "-15x^5+25x^4-9x^3+27x^1+3"};
		String[] output = {"36x^5-64x^3+4x^1+3", "-10x^1", "-75x^4+100x^3-27x^2+27"};
		for (int i = 0; i < 3; i++){
			Polynomial p1 = new Polynomial(s1[i]);
			Polynomial p3 = op.derPol(p1);
			assertEquals(p3.toString(), output[i]);
		}
	}


	@Test
	public void test6() {
		Operations op = new Operations();
		String[] s1 = {"+15x^3-2x^1+9","-9x^2+9", "-15x^5+25x^4-9x^3+27x^1+3"};
		String[] output = {"3.75x^4-1x^2+9x^1", "-3x^3+9x^1", "-2.5x^6+5x^5-2.25x^4+13.5x^2+3x^1"};
		for (int i = 0; i < 1; i++){
			Polynomial p1 = new Polynomial(s1[i]);
			Polynomial p3 = op.integrPol(p1);
			assertEquals(p3.toString(), output[i]);
		}
	}
}
