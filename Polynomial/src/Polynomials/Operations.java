package Polynomials;

public class Operations {

	//add two polynomials
	public Polynomial addPol(Polynomial P1, Polynomial P2){
		Polynomial P3 = new Polynomial();
		for(Monome m: P1.P){
			MonomeI m3 = new MonomeI(m.getCoeff().intValue(), m.getGrade());
			P3.P.add(m3);
		}
		for(Monome m: P2.P){
			boolean newm = true;
			for (Monome mo: P3.P){
				if(mo.getGrade() == m.getGrade()){
					mo.setCoeff(mo.getCoeff().intValue() + m.getCoeff().intValue());
					newm = false;
				}
			}
			if(newm == true){
				MonomeI m3 = new MonomeI(m.getCoeff().intValue(), m.getGrade());
				P3.P.add(m3);
			}
				
		}			
	P3.sortPol();
	return P3;
	}
	
	//substracts two polynomials
	public Polynomial subPol(Polynomial P1, Polynomial P2){
		Polynomial P3 = new Polynomial();
		for(Monome m: P1.P){
			if(m instanceof MonomeI){
				MonomeI m3 = new MonomeI(m.getCoeff().intValue(), m.getGrade());
				P3.P.add(m3);
			}
			else{
				MonomeD m3 = new MonomeD(m.getCoeff().doubleValue(), m.getGrade());
				P3.P.add(m3);
			}
		}
		for(Monome m: P2.P){
				boolean newm = true;
				for (Monome mo: P3.P){
					if (mo instanceof MonomeI){
						if(mo.getGrade() == m.getGrade()){
							mo.setCoeff(mo.getCoeff().intValue() - m.getCoeff().intValue());
							newm = false;
						}
					}else{
						if(mo.getGrade() == m.getGrade()){
							mo.setCoeff(mo.getCoeff().doubleValue() - m.getCoeff().doubleValue());
							newm = false;
						}	
						}
							
				}
				if(newm == true){
					MonomeI m3 = new MonomeI(-m.getCoeff().intValue(), m.getGrade());
					P3.P.add(m3);
				}
			}		
	P3.sortPol();
	return P3;
	}
	
	//multiplies two polynomials
	public Polynomial mulPol(Polynomial P1, Polynomial P2){
		Polynomial P3 = new Polynomial();
		for(Monome m: P1.P){
			for (Monome m2: P2.P){
				MonomeI m3 = new MonomeI(m.getCoeff().intValue()*m2.getCoeff().intValue(), m.getGrade()+m2.getGrade());
				boolean newm = true;
				for (Monome m4: P3.P){
					if(m4.getGrade() == m3.getGrade()){
						m4.setCoeff(m3.getCoeff().intValue() * m4.getCoeff().intValue());
						newm = false;
					}
				}
				if (newm == true)
					P3.P.add(m3);
			}
		}
		P3.sortPol();
		return P3;
	}
	
	//derivates a polynomial
	public Polynomial derPol(Polynomial P1){
		for (Monome m: P1.P){
			m.setCoeff(m.getCoeff().intValue()*m.getGrade());
			m.setGrade(m.getGrade()-1);
		}
		return P1;
	}
	
	//integrates a polynomial
	public Polynomial integrPol(Polynomial P1){
		Polynomial P3 = new Polynomial();
		for (Monome m: P1.P ){
			if(m.getGrade() != 0){
				if((m.getCoeff().doubleValue() % (m.getGrade()+1)) == 0){
					MonomeI m2 = new MonomeI(m.getCoeff().intValue()/(m.getGrade()+1), m.getGrade()+1);
					P3.P.add(m2);
				}
				else{
					MonomeD m2 = new MonomeD(m.getCoeff().doubleValue()/(m.getGrade()+1), m.getGrade()+1);
					P3.P.add(m2);
				}
			}
			else{
				MonomeI m2 = new MonomeI(m.getCoeff().intValue(), m.getGrade()+1);
				P3.P.add(m2);
			}
				
		}
		P3.sortPol();
		return P3;
	}
	
	//divides two polynomials
	public Polynomial[] divPol(Polynomial P1, Polynomial P2){
		Polynomial[] cr = new Polynomial[3];
		Polynomial P3 = new Polynomial();
		boolean nulli = true;
		P1.sortPol(); P2.sortPol();
		int grade1 = P1.P.get(0).getGrade();
		int grade2 = P2.P.get(0).getGrade();
		double coeff1 = P1.P.get(0).getCoeff().intValue();
		double coeff2 = P2.P.get(0).getCoeff().intValue();
		while(grade1 >= grade2){
			nulli = true;
			Polynomial Pr = new Polynomial(); 
			if (coeff1 % coeff2 == 0){
				MonomeI m = new MonomeI((int) (coeff1/coeff2), grade1-grade2);
				P3.P.add(m);
				for (Monome m1 : P2.P){
					if(m1 instanceof MonomeI){
						MonomeI m2 = new MonomeI(m1.getCoeff().intValue()*m.getCoeff().intValue(), m.getGrade()+m1.getGrade());
						Pr.P.add(m2);
					}
					else{
						MonomeD m2 = new MonomeD(m1.getCoeff().doubleValue()*m.getCoeff().doubleValue(), m.getGrade()+m1.getGrade());
						Pr.P.add(m2);
					}
				}
			}
			else{
				MonomeD m = new MonomeD((coeff1/coeff2), grade1-grade2);
				P3.P.add(m);
				for (Monome m1 : P2.P){
						MonomeD m2 = new MonomeD(m1.getCoeff().doubleValue()*m.getCoeff().doubleValue(), m.getGrade()+m1.getGrade());
						Pr.P.add(m2);
					}
				}
			Polynomial Pr2 = new Polynomial();
			Pr2 = subPol(P1, Pr);
			Pr2.sortPol();
			for(Monome m3: Pr2.P)
				if(m3.getCoeff().doubleValue() != 0 ){
					coeff1 = m3.getCoeff().doubleValue();
					grade1 = m3.getGrade();
					nulli = false;
					break;
				}
			if(nulli == true)
				grade1 = 0;
			P1 = Pr2;
		}
		cr[0] = P3;
		if(nulli == false)
			cr[1] = P1;
		else cr[1] = new Polynomial("+0x^0");
		return cr;
		
	}
}

