package Polynomials;
import java.util.ArrayList;
import java.util.Collections;

public class Polynomial {
	public ArrayList<Monome> P;
	private String string="";
	
	
	public Polynomial(String s){
		P = new ArrayList<Monome>();
		createP(s);
		
	}
	
	Polynomial(){
		ArrayList<Monome> P = new ArrayList<Monome>();
		this.P = P;
	}
	
	//receives the string form of a polynomial and it splits it into monomyals 
	public void createP(String s){ 
		int i; int coeff; boolean free = false;
		int grade;
		for (i = 0; i < s.length()-1; i++){
			coeff = 0; grade = 0; free = false;
			if(s.charAt(i) == '+'){
				while (s.charAt(i+1) != 'x'){
					coeff = coeff*10 + Integer.parseInt(s.substring(i+1, i+2));
					i++;
				}
				i = i + 2;
			}
			if(s.charAt(i) == '-'){
				while (s.charAt(i+1) != 'x'){
					coeff = coeff*10 + Integer.parseInt(s.substring(i+1, i+2));
					i++;
				}
				coeff = -coeff; i = i + 2;
			}
			if(s.charAt(i) == '^'){
				while(i+1 < s.length() && s.charAt(i+1) != '+' && s.charAt(i+1) != '-'){
						grade = grade*10 + Integer.parseInt(s.substring(i+1, i+2));
						i++;
				}
			}
			MonomeI m = new MonomeI(coeff, grade);
			P.add(m);
			for (int j = i; j < s.length(); j++){
				if (s.charAt(j) == 'x'){
					free = true;
					break;
				}
			}
			if (free == false)
				break;
		}
		if (free == false && i != s.length()-1){
			coeff = 0;
			if (s.charAt(s.length()-2) == '+')
				coeff = Integer.parseInt(s.substring(s.length()-1, s.length()));
			else 
				coeff = -Integer.parseInt(s.substring(s.length()-1, s.length()));
			grade = 0;
			MonomeI m = new MonomeI(coeff, grade);
			P.add(m);
		}
		
	}
	
	//arranges the monomyals of a polynomial in ascending order by their grades
	public void sortPol(){
		MonomeComparator comparator = new MonomeComparator();
		Collections.sort(this.P, Collections.reverseOrder(comparator));
	}
	
	//creates the string form of a polynomial to be displayed in the FieldText
	public String toString(){
		for(Monome m: this.P){
			if (m.getCoeff().doubleValue() > 0 && m != this.P.get(0))
				string = string + "+";
			if (m.getGrade() == 0 && m.getCoeff().doubleValue() != 0)
				string = string + m.getCoeff();
			if(m.getCoeff().doubleValue() != 0 && m.getGrade() != 0)
				string = string + m.getCoeff() + "x^" + m.getGrade();
			
			
		}
		return string;
	}

	
}
