package Polynomials;

public abstract class Monome {
	protected Number coeff;
	protected int grade;
	
	Monome(Number coeff, int grade){
		this.coeff = coeff;
		this.grade = grade;
	}
	
	public Number getCoeff(){
		return this.coeff;
	}
	
	public int getGrade(){
		return this.grade;
	}
	
	public void setCoeff(Number coeff){
		this.coeff = coeff;
	}
	
	public void setGrade(int grade){
		this.grade = grade;
	}
}
