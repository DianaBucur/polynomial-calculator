package Polynomials;
import java.util.*;

public class MonomeComparator implements Comparator<Monome> {

	public int compare(Monome m1, Monome m2) {
		return Float.compare(m1.getGrade(),m2.getGrade());
	}

}
